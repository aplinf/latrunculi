use std::cmp::max;

use super::{
    Game,
    Move,
    MAX_NO_TAKE_COUNT
};


// Minimax
const MAX: i8 = 100;
const MEAN_MOVE: i8 = 50;
const DEFAULT_DEPTH: u8 = 4;

impl Game {
    pub fn best_move(&mut self) -> Option<Move> {
        let depth = match self.player_turn {
            -1    => if let Some(a) = self.player_mode[0].as_ref() {*a} else {DEFAULT_DEPTH},
             1| _ => if let Some(a) = self.player_mode[1].as_ref() {*a} else {DEFAULT_DEPTH},
        };

        // self.print_dbg(format!(
        //     "O.. depth: {}, player: {:2}, ttl: {:2}, diff: {}\n",
        //     depth,
        //     self.player_turn,
        //     self.no_take_count,
        //     depth,
        // ).as_str(), 1);
        // self.print_state(1);

        let mut best_rating = -MAX;
        let mut best_move = None;

        let players_stones = self.board.get_stone_positions(self.player_turn);

        for opt in players_stones.iter() {
            if let &Some(position) = opt {
                let moves = self.board.get_moves(position);

                for i in moves.iter() {
                    if let &Some(move_n) = i {
                        self.play_move(move_n, true);
                        let rating =  -self.minimax(depth - 1);

                        self.print_dbg(format!("Move variant: {:?} = {}\n", move_n, rating).as_str(), 2);

                        self.revert_move(true);

                        if rating > best_rating {
                            best_rating = rating;
                            best_move = Some(move_n);
                        }
                    }
                }
            } else {break}
        }

        best_move
    }

    fn minimax(&mut self, depth: u8) -> i8 {
        self.print_dbg(format!("O.. dp: {}, player: {}\n", depth, self.player_turn).as_str(), 3);
        self.print_state(3);

        if self.is_loss() {
            return -MAX
        }
        if self.is_win() {
            return MAX
        }
        if self.is_draw() {
            return 0
        }

        if depth == 0 {
            let rating = self.rate_position();

            return rating;
        } else {
            let mut rating = -MAX;

            for opt in self.board.get_stone_positions(self.player_turn).iter() {
                if let &Some(position) = opt {
                    let moves = self.board.get_moves(position);

                    for i in moves.iter() {
                        if let &Some(move_n) = i {
                            self.play_move(move_n, true);
                            rating = max(rating, -self.minimax(depth - 1));

                            self.print_dbg(format!("{} Move variant: {:?} = {}\n", depth, move_n, rating).as_str(), 2);

                            self.revert_move(true);
                        }
                    }
                }
            }

            if rating > MEAN_MOVE {
                rating -= 1;
            }

            if rating < MEAN_MOVE {
                rating += 1;
            }

            return rating;
        }
    }

    fn rate_position(&self) -> i8 {
        let score_part = match self.player_turn {
            -1 => {
                let mut rate = self.board.score[0] as i8 * 2 - self.board.score[1] as i8 * 4;

                rate
            },
             1 => {
                let mut rate = self.board.score[1] as i8 * 2 - self.board.score[0] as i8 * 4;

                rate
             },
            // -1 => -(self.board.score[1] as i8) * 4,
            //  1 => -(self.board.score[0] as i8) * 4,
             _ => 0,
        };

        //let mut aggression = 0;

        let progress = match self.player_turn {
            -1 => {
                let mut progress = 0;

                for row in {0..self.board.matrix.len()} {
                    for col in {0..self.board.matrix[row].len()} {
                        if self.board.matrix[row][col] == -1 {
                            progress += row;

                        }
                    }
                }
                progress
            },
            1 => {
                let mut progress = 0;

                for row in {0..self.board.matrix.len()} {
                    for col in {0..self.board.matrix[row].len()} {
                        if self.board.matrix[row][col] == 1 {
                            progress += self.board.matrix.len() - 1 - row;
                        }
                    }
                }
                progress
            },
            _ => 0,
        };



        score_part + progress as i8
    }

    fn is_loss(&self) -> bool {
        if self.player_turn == -1 && (self.board.score[0] == 0 ||
            (self.no_take_count >= MAX_NO_TAKE_COUNT &&
            self.board.score[0] < self.board.score[1])) ||
            self.player_turn == 1 && (self.board.score[1] == 0 ||
            (self.no_take_count >= MAX_NO_TAKE_COUNT &&
            self.board.score[1] < self.board.score[0])) {
                // println!("LOSS!!!");
                true
            } else {false}
    }

    fn is_win(&self) -> bool {
        if self.player_turn == -1 && (self.board.score[1] == 0 ||
            (self.no_take_count >= MAX_NO_TAKE_COUNT &&
            self.board.score[0] > self.board.score[1])) ||
            self.player_turn == 1 && (self.board.score[0] == 0 ||
            (self.no_take_count >= MAX_NO_TAKE_COUNT &&
            self.board.score[1] > self.board.score[0])) {
                // println!("WIN!!!");
                true
            } else {false}
    }

    fn is_draw(&self) -> bool {
        if self.no_take_count >= MAX_NO_TAKE_COUNT &&
            self.board.score[0] == self.board.score[1] {
            // println!("Draw!!!");
            true
        } else {false}
    }
}
