use serde::{Serialize, Deserialize};

mod board;
mod history;
pub mod utils;
mod minimax;

pub use board::Board;
use board::SURROUNDING;
pub use history::{History, HistItem};

const DEBUG: u8 = 0;

const MAX_NO_TAKE_COUNT: u8 = 30;
// const MAX_DIFFICULTY: u8 = 5;
const DEFAULT_PLAYER_MODE: [Option<u8>; 2] = [None, None];

type Pos = [i8; 2];
pub type Move = [Pos; 2];

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Game {
    pub board: Board,
    pub no_take_count: u8,      // 30 -> winner by higher score
    pub player_turn: i8,
    pub history: History,
    pub player_mode: [Option<u8>; 2],
}

// play
impl Game {
    pub fn play_move(&mut self, move_n: Move, history: bool) -> Option<i8> {
        let no_take_count = self.no_take_count;
        self.board.move_stone(&move_n);

        let taken = self.board.scan_and_take(move_n[1], self.player_turn);

        if taken.iter().any(|&i| i != 0) {
            self.no_take_count = 0;
        } else {
            self.no_take_count += 1;
        }

        // DBG
        self.print_dbg(format!("Playing({:2}): {:?}, {:?}, {:?}\n", self.player_turn, move_n, taken, no_take_count).as_str(), 3);

        if history {
            self.history.push((move_n, taken, no_take_count, self.player_turn));
        } else {
            self.history.next();
        }

        self.toggle_player();

        self.is_game_over()
    }

    fn revert_move(&mut self, history: bool) {
        if let Some((move_n, taken, no_take_count, _player_turn))
               = if history {self.history.pop()} else {self.history.prev()}
        {
            self.print_dbg(format!("Reverting: {:?}, {:?}, {:?}\n", move_n, taken, no_take_count).as_str(), 3);

            self.board.move_stone(&[move_n[1], move_n[0]]);

            for (idx, taken_stone) in taken.iter().enumerate() {
                if *taken_stone != 0 {
                    self.board.untake_stone(
                        &[move_n[1][0] + SURROUNDING[idx][0], move_n[1][1] + SURROUNDING[idx][1]],
                        *taken_stone
                    );
                }
            }

            self.no_take_count = no_take_count;
            self.toggle_player();
        }
    }

    // pub async fn play_move_ai(&mut self) -> Option<i8> {
    //     if let Some(best_move) = self.best_move() {
    //         if self.is_game_over().is_none() {
    //             return self.play_move(best_move, true);
    //         }
    //     }
    //
    //     None
    // }

    /// On cell selection: [select a cell, unselect a cell, play a move]
    ///     returns true if a move was done
    pub fn select(&mut self, selected: Pos) -> Option<Option<i8>> {
        if self.is_game_over().is_none() {
            let cell_cont = self.board.get_cell(&selected);

            if let Some(prev_selected) = self.board.selected {
                if prev_selected == selected {
                    self.board.selected = None;
                } else {
                    let move_n = [prev_selected, selected];

                    if self.board.is_empty_cell(&selected) &&
                    Board::is_legal_move(move_n) {
                        self.play_move(move_n, true);
                        self.board.selected = None;
                        self.board.proposed = None;
                        return Some(self.is_game_over());
                    }
                }
            } else {
                if self.is_current_players_stone(cell_cont) {
                    self.board.selected = Some(selected);
                }
            }
        }

        None
    }

}

impl Game {
    pub fn new() -> Self {
        let game = Self {
            board: Board::new(),
            no_take_count: 0,
            player_turn: -1,
            history: History::new(),
            player_mode: DEFAULT_PLAYER_MODE,
        };

        game
    }

    pub fn reinit(&mut self) {
        // self = Game::new();
        self.board = Board::new();
        self.no_take_count = 0;
        self.player_turn = -1;
        self.history = History::new();
        // self.debbug_buff = ;
        self.player_mode = DEFAULT_PLAYER_MODE;
    }

    pub fn import(&mut self, buf: String) -> Result<(), Box<dyn std::error::Error>> {
        let imported_game: Game = serde_json::from_str(&utils::verify(buf)?)?;

        // self = Game::new();
        self.board = imported_game.board;
        self.no_take_count = imported_game.no_take_count;
        self.player_turn = imported_game.player_turn;
        self.history = imported_game.history;
        // self.debbug_buff = ;
        self.player_mode = imported_game.player_mode;

        Ok(())
    }

    pub fn export(&self) -> String {
        let game_json = self.serialize();

        utils::sign(game_json)
    }

    pub fn hint(&mut self) {
       let best_move = self.best_move();

       self.board.proposed = best_move;
    }

    pub fn set_history(&mut self, selected_idx: i32) {
        let hist_current_step = self.history.current_step;

        let diff = (hist_current_step - selected_idx).abs();

        if selected_idx < hist_current_step {
            for _i in 0..diff {
                self.revert_move(false);
            }
        } else if selected_idx > hist_current_step {
            for _ in 0..diff {
                self.play_move(self.history.get_by_index(self.history.current_step + 1).0, false);
            }
        }
    }

    fn toggle_player(&mut self) -> i8 {
        if self.player_turn == -1 {
            self.player_turn = 1;
            1
        } else {
            self.player_turn = -1;
            -1
        }
    }

    pub fn is_current_players_stone(&self, player: i8) -> bool {
        if self.player_turn == player {true} else {false}
    }

    pub fn is_game_over(&self) -> Option<i8> {
        if self.board.score[0] == 0 {
            return Some(1);
        }
        if self.board.score[1] == 0 {
            return Some(-1);
        }

        if self.no_take_count == 30 {
            if self.board.score[0] > self.board.score[1] {
                return Some(-1);
            } else if self.board.score[0] < self.board.score[1] {
                return Some(1);
            } else {
                return Some(0);
            }
        }

        None
    }

    pub fn is_player_ai(&self) -> bool {
        match self.player_turn {
            -1 => self.player_mode[0].is_some(),
             1 => self.player_mode[1].is_some(),
             _ => false,
        }
    }

    pub fn print_dbg(&self, str: &str, level: u8) {
        if DEBUG >= level {
            println!("{}", str);
        }
    }

    fn print_state(&self, level: u8) {
        if DEBUG >= level {
            self.print_dbg(format!("{}------------------------{}\n", self.board.score[0], self.board.score[1]).as_str(), 0);
            for row in self.board.matrix.iter() {
                self.print_dbg("[", 0);

                for cell in row.iter() {
                    self.print_dbg(format!("{:2},", cell).as_str(), 0);
                }

                self.print_dbg("],\n", 0);
            }

            self.print_dbg(format!("---------------------------\n\n").as_str(), 0);
        }
    }
    pub fn get_player_mode(&self) -> [Option<u8>; 2] {
        self.player_mode
    }

    pub fn set_player_mode(&mut self, player_mode: (i8, Option<u8>)) {

        match player_mode.0 {
            -1 => self.player_mode[0] = player_mode.1,
            1 => self.player_mode[1] = player_mode.1,
             _ => (),
        }
        println!("set {:?}", self.player_mode);
    }

    pub fn get_game_info(&self) -> GameInfo {
        GameInfo {
            score: self.board.score,
            player_turn: self.player_turn,
            no_take_count: self.no_take_count,
            history: self.history.clone(),
            player_mode: self.player_mode,
            // no_take_count: self.get_winner()
        }
    }

    pub fn serialize(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}

pub struct GameInfo {
    pub score: [u8; 2],
    pub player_turn: i8,
    pub no_take_count: u8,
    pub history: History,
    pub player_mode: [Option<u8>; 2],
}

impl GameInfo {
    pub fn get_player_mode(&self, player: i8) -> String {
        if let Some(depth) = self.player_mode[player_idx(player)] {
            depth.to_string()
        } else {
            "None".to_owned()
        }
    }
}

fn player_idx(player: i8) -> usize {
    match player {
        -1 => 0,
         1 | _ => 1,
    }
}
