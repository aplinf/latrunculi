pub type BoardMatrix = [[i8; 8]; 7];

pub static BOARDS: [(&'static str, BoardMatrix); 5] = [
    (
        "Openeing",
        [
            [-1,-1,-1,-1,-1,-1,-1,-1],
            [-1,-1,-1,-1,-1,-1,-1,-1],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 1, 1, 1, 1, 1, 1, 1, 1],
            [ 1, 1, 1, 1, 1, 1, 1, 1],
        ]
    ),
    (
        "Fig. 1.",
        [   [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0,-1, 0,-1, 0, 0, 0],
            [-1, 1, 0, 1, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0], ]
    ),
    (
        "Fig. 2.",
        [   [ 0, 0, 0, 0, 1, 0, 0, 0],
            [ 0, 0,-1, 0, 0, 0,-1, 0],
            [ 0, 0, 0, 0,-1, 0, 0, 1],
            [ 0,-1, 0, 1, 0, 0, 0, 1],
            [ 0, 1, 0, 0, 0, 0, 1, 0],
            [ 0,-1, 0,-1, 0,-1,-1, 0],
            [ 0, 0, 0, 0,-1, 0, 0,-1], ]
    ),
    (
        "Corners",
        [   [ 1, 0,-1, 0, 0, 0,-1, 1],
            [-1, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0,-1],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [-1, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0,-1],
            [ 1,-1, 0, 0, 0,-1, 0, 1], ]
    ),
    (
        "Simple",
        [   [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 1,-1, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 1, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0], ]
    ),
];
