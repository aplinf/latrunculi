// use std::fs::{OpenOptions, File};
// use std::os::unix::fs::OpenOptionsExt;
// use std::io::stdin;
//
// use libc;
// use nix::unistd;
// use nix::sys::stat;
// use tempfile::tempdir;
use std::{thread, time};

use sha2::Sha256;
use hmac::{Hmac, Mac, NewMac};


type HmacSha256 = Hmac<Sha256>;
const SECRET: &str = "4a3484b3bf2fcb0ff5f717f18904abd29d228bc13098114df68b2ed9a4626503";

pub fn sign(mut input: String) -> String {
    let mut mac = HmacSha256::new_varkey(SECRET.as_bytes()).unwrap();

    mac.update(input.as_bytes());

    let signature = mac.finalize().into_bytes();

    input.push_str(".");
    input.push_str(&base64::encode(&signature));

    input
}

pub fn verify(mut input: String) -> Result<String, &'static str> {
    let mut mac = HmacSha256::new_varkey(SECRET.as_bytes()).unwrap();

    let delimiter_idx = input.rfind('.')
        .ok_or("signature delimiter not found")?;

    let signature = input.split_off(delimiter_idx);
    let signature = base64::decode(signature.trim_end()[1..].as_bytes())
        .or(Err("corrupted signature"))?;

    mac.update(input.as_bytes());

    match mac.verify(&signature) {
        Ok(_) => Ok(input),
        Err(_) => Err("invalid signature"),
    }
}




pub fn player_to_char(player: i8) -> char {
    match player {
        -1 => '⚫',
         1 => '⚪',
         _ => ' ',
    }
}

pub fn sleep(duration: u64) {
    let (tx, rx) = flume::unbounded();
    let duration = time::Duration::from_secs(duration);

    thread::spawn(move || {

        thread::sleep(duration);
        tx.send(()).unwrap();
    });

    rx.recv().unwrap()
}
