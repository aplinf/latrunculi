use serde::{Serialize, Deserialize};

use super:: Move;

pub type HistItem = (Move, [i8; 8], u8, i8);

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct History {
    pub current_step: i32,
    pub steps: Vec<HistItem>,
}

impl History {
    pub fn new() -> Self {
        Self {
            current_step: -1,
            steps: Vec::with_capacity(50),
        }
    }

    pub fn push(&mut self, item: HistItem) {
        if self.current_step != self.last_idx() {
            self.steps.truncate((self.current_step + 1) as usize);
        }

        self.steps.push(item);
        self.current_step += 1;
    }

    pub fn pop(&mut self) -> Option<HistItem> {
        if self.current_step >= 0 {
            self.current_step -= 1;
            self.steps.pop()
        } else {
            return None
        }
    }

    pub fn next(&mut self) {
        self.current_step += 1;
    }

    pub fn prev(&mut self) -> Option<HistItem> {
        if self.current_step >= 0 {
            let item = Some(self.steps[self.current_step as usize]);
            self.current_step -= 1;

            item
        } else {
            return None
        }
    }

    pub fn get_by_index(&self, idx: i32) -> &HistItem {
        &self.steps[idx as usize]
    }

    pub fn last(&self) -> Option<&HistItem>{
        self.steps.last()
    }

    pub fn last_idx(&self) -> i32 {
        (self.steps.len()) as i32 - 1
    }
}
