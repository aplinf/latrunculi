use serde::{Serialize, Deserialize};

use super::{Pos, Move};
type Score = [u8; 2];

pub type BoardMatrix = [[i8; 8]; 7];
pub const SURROUNDING: [Pos; 8] = [[-1, -1],[0, -1],[1, -1],[-1, 0],[1, 0],[-1, 1],[0, 1],[1, 1]];
const MOVE_SHIFT: [[i8; 2]; 4] = [[0, -1], [1, 0], [0, 1], [-1, 0]];


// const BOARD_MATRIX: [[i8; 8]; 7] = [
//     [ 1, 0,-1, 0, 0, 0,-1, 1],
//     [-1, 0, 0, 0, 0, 0, 0, 0],
//     [ 0, 0, 0, 0, 0, 0, 0,-1],
//     [ 0, 0, 0, 0, 0, 0, 0, 0],
//     [-1, 0, 0, 0, 0, 0, 0, 0],
//     [ 0, 0, 0, 0, 0, 0, 0,-1],
//     [ 1,-1, 0, 0, 0,-1, 0, 1],
// ];
const BOARD_MATRIX: [[i8; 8]; 7] = [
    [-1,-1,-1,-1,-1,-1,-1,-1],
    [-1,-1,-1,-1,-1,-1,-1,-1],
    [ 0, 0, 0, 0, 0, 0, 0, 0],
    [ 0, 0, 0, 0, 0, 0, 0, 0],
    [ 0, 0, 0, 0, 0, 0, 0, 0],
    [ 1, 1, 1, 1, 1, 1, 1, 1],
    [ 1, 1, 1, 1, 1, 1, 1, 1],
];

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Board {
    pub matrix: BoardMatrix,
    pub score: Score,
    // hovered: Pos,
    pub selected: Option<Pos>,
    pub proposed: Option<Move>
}

impl Board {
    pub fn new() -> Self {
        let matrix = BOARD_MATRIX;

        let mut score = [0, 0];
        for row in matrix.iter() {
            for cell in row.iter() {
                match cell {
                    -1 => score[0] += 1,
                     1 => score[1] += 1,
                     _ => (),
                }
            }
        }

        Self {
            matrix,
            score,
            // hovered: [4, 1],
            selected: None,
            proposed: None,
        }
    }

    pub fn get_cell(&self, pos: &Pos) -> i8 {
        self.matrix[pos[1] as usize][pos[0] as usize]
    }

    fn set_cell(&mut self, pos: &Pos, stone: i8) {
        self.matrix[pos[1] as usize][pos[0] as usize] = stone;
    }

    pub fn get_stone_positions(&self, player: i8) -> [Option<Pos>; 16] {
        let mut positions = [None; 16];
        let mut i = 0;

        for (row_idx, row) in self.matrix.iter().enumerate() {
            for (col_idx, cell) in row.iter().enumerate() {
                if *cell == player {
                    positions[i] = Some([col_idx as i8, row_idx as i8]);
                    i += 1;
                }
            }
        }

        positions
    }

    pub fn get_moves(&self, from_pos: Pos) -> [Option<Move>; 4] {
        let mut moves = [None; 4];

        for (idx, shift) in MOVE_SHIFT.iter().enumerate() {
            let to_pos = [from_pos[0] + shift[0], from_pos[1] + shift[1]];

            if self.is_empty_cell(&to_pos) {
                moves[idx] = Some([from_pos, to_pos]);
            } else {
                moves[idx] = None
            }
        }

        moves
    }

    pub fn scan_and_take(&mut self, pos: Pos, player: i8) -> [i8; 8] {
        let mut taken_stones = [0; 8];

        for (idx, shift) in SURROUNDING.iter().enumerate() {
            let shift_pos = [pos[0] + shift[0], pos[1] + shift[1]];

            if self.is_enemy_cell(&shift_pos, player) {
                if match shift_pos {
                    [0, 0] => self.is_friendly_cell(&[0, 1], player) &&
                              self.is_friendly_cell(&[1, 0], player),
                    [7, 0] => self.is_friendly_cell(&[6, 0], player) &&
                              self.is_friendly_cell(&[7, 1], player),
                    [7, 6] => self.is_friendly_cell(&[7, 5], player) &&
                              self.is_friendly_cell(&[6, 6], player),
                    [0, 6] => self.is_friendly_cell(&[0, 5], player) &&
                              self.is_friendly_cell(&[1, 6], player),
                    _ => false,
                } {
                    self.take_stone(&shift_pos);
                    taken_stones[idx] = player * -1;
                } else {
                    let opposite = [2 * shift_pos[0] - pos[0], 2 * shift_pos[1] - pos[1]];

                    if self.is_friendly_cell(&opposite, player) {
                        self.take_stone(&shift_pos);
                        taken_stones[idx] = player * -1;
                    }
                }

            }
        }

        taken_stones
    }

    pub fn take_stone(&mut self, pos: &Pos) {
        let stone =  self.get_cell(&pos);
        self.set_cell(&pos, 0);

        match stone {
            -1 => self.score[0] -= 1,
             1 => self.score[1] -= 1,
             _ => panic!("Can not take empty cell"),
        }
    }

    pub fn untake_stone(&mut self, pos: &Pos, stone: i8) {
        self.set_cell(&pos, stone);

        match stone {
            -1 => self.score[0] += 1,
             1 => self.score[1] += 1,
             _ => panic!("Can not untake nothing"),
        }
    }

    pub fn move_stone(&mut self, move_n: &Move) {
        let stone =  self.get_cell(&move_n[0]);
        self.set_cell(&move_n[0], 0);
        self.set_cell(&move_n[1], stone);

    }

    pub fn is_empty_cell(&self, pos: &Pos) -> bool {
        if Board::is_cell(pos[0], pos[1]) &&
           self.matrix[pos[1] as usize][pos[0] as usize] == 0 {
            true
        } else {false}
    }

    pub fn is_friendly_cell(&self, pos: &Pos, player: i8) -> bool {
        if Board::is_cell(pos[0], pos[1]) &&
           self.matrix[pos[1] as usize][pos[0] as usize] == player {
            true
        } else {false}
    }

    pub fn is_enemy_cell(&self, pos: &Pos, player: i8) -> bool {
        if Board::is_cell(pos[0], pos[1]) &&
           self.matrix[pos[1] as usize][pos[0] as usize] == (player * -1) {
            true
        } else {false}
    }

    pub fn set_proposed(&mut self, move_opt: Option<Move>) {
        self.proposed = move_opt;
    }
}

impl Board {
    pub fn is_legal_move(move_n: Move) -> bool {
        let from = &move_n[0];
        let to = &move_n[1];

        if to[0] == from[0] + 1 && to[1] == from[1]     ||
           to[0] == from[0]     && to[1] == from[1] - 1 ||
           to[0] == from[0] - 1 && to[1] == from[1]     ||
           to[0] == from[0]     && to[1] == from[1] + 1
         { true }
        else { false }
    }

    fn is_cell(x: i8, y: i8) -> bool {
        if x < 0 || x > 7 ||
           y < 0 || y > 6 {
               false
           } else { true }

    }
}
