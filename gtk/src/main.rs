#![allow(unknown_lints)]
#![allow(option_map_unit_fn)]

#[macro_use]
extern crate cascade;

use latrunculi::Game;

mod ui;
pub mod game_worker;

use self::ui::{App, State, AppEvent, Button};
use std::process;

fn main() {
    glib::set_program_name("Latrunculi".into());
    glib::set_application_name("Latrunculi");

    if gtk::init().is_err() {
        eprintln!("failed to initialize GTK Application");
        process::exit(1);
    }

    let (tx, rx) = flume::unbounded();

    let mut app = App::new(State {
        app_tx: tx.clone(),
        game: Game::new(),
        is_paused: true,
    });


    // let (tx2, rx2) = flume::unbounded();
    //
    // let move_ai = async move {
    //     while let Ok(()) = rx2.recv_async().await {
    //         &app.try_play_ai();
    //     }
    // };



    let event_handler = async move {
        app.connect_events(tx);
        app.show();

        while let Ok(event) = rx.recv_async().await {
            match event {
                AppEvent::SelectBoard(cursor_pos) => app.select_board_cell(cursor_pos),
                // AppEvent::GetBestMove => app.get_best_move(),
                AppEvent::ShowHint(move_n) => app.show_hint(move_n),
                AppEvent::PlayAI => app.play_ai(),
                AppEvent::PlayMove(move_n) => app.play_move(move_n),
                AppEvent::SelectHistory(idx) => app.select_history(idx),
                AppEvent::SetPlayerMode(player_mode) => app.set_player_mode(player_mode),
                AppEvent::FileDialog(action) => app.file_dialog(action),
                AppEvent::OpenGame(path) => app.open_game(path),
                AppEvent::SaveGame(path) => app.save_game(path),
                AppEvent::ClickButton(button) => match button {
                    Button::Hint => app.get_best_move_and(AppEvent::ShowHint(None)),
                    Button::NewGame => app.new_game(),
                    Button::Undo => app.undo(),
                    Button::Redo => app.redo(),
                },
                AppEvent::Update => app.update(),
                AppEvent::PlayPause => app.toggle_play_pause(),
                // _ => ()
            }
        }
    };


    glib::MainContext::default().spawn_local(
        // async move {
        //     futures::future::join(
        //         event_handler,
        //         move_ai
        //     ).await;
        // }
        event_handler
    );

    gtk::main();
}


// const BOARD_MATRIX: [[i8; 8]; 7] = [
//     [ 1, 1, 1, 1, 1, 1, 1, 1],
//     [ 1, 1, 1, 1, 1, 1, 1, 1],
//     [ 0, 0, 0, 0, 0, 0, 0, 0],
//     [ 0, 0, 0, 0, 0, 0, 0, 0],
//     [ 0, 0, 0, 0, 0, 0, 0, 0],
//     [-1,-1,-1,-1,-1,-1,-1,-1],
//     [-1,-1,-1,-1,-1,-1,-1,-1],
// ];
