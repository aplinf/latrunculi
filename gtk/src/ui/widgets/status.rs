use gtk::prelude::*;
use gtk::{
    Align,
    Grid,
    Label,
    ComboBoxText,
    Button,
};

use latrunculi::{utils, GameInfo};


pub struct Status {
    pub container: Grid,
    info_player: Label,
    info_nocapture: Label,
    info_black: Label,
    info_white: Label,
    pub combo_black_mode: ComboBoxText,
    pub combo_white_mode: ComboBoxText,
    pub button_play_pause: Button,
}

impl Status {
    pub fn new() -> Self {
        // Player turn
        let label_name_player = cascade!{
            Label::new(Some("Player:"));
            ..set_halign(Align::End);
        };
        let label_value_player = cascade!{
            Label::new(Some(&"⚫"));
            ..set_halign(Align::Start);
        };

        // Black
        let label_name_black = cascade!{
            Label::new(Some("Black stones:"));
            ..set_halign(Align::End);
        };
        let label_value_black = cascade!{
            Label::new(Some("0"));
            ..set_halign(Align::Start);
        };

        // White
        let label_name_white = cascade!{
            Label::new(Some("White stones:"));
            ..set_halign(Align::End);
        };
        let label_value_white = cascade!{
            Label::new(Some("0"));
            ..set_halign(Align::Start);
        };

        // No capture
        let label_name_nocapture = cascade!{
            Label::new(Some("No capture:"));
            ..set_halign(Align::End);
        };
        let label_value_nocapture = cascade!{
            Label::new(Some("0"));
            ..set_halign(Align::Start);
        };

        // AI black
        let label_name_black_mode = cascade!{
            Label::new(Some("AI Black:"));
            ..set_halign(Align::End);
        };
        let label_value_black_mode = cascade!{
            ComboBoxText::new();
            ..append(Some(&"None"), &"Human");
            ..append(Some(&"3"), &"AI Easy");
            ..append(Some(&"4"), &"AI Medium");
            ..append(Some(&"5"), &"AI Difficult");
            ..set_active_id(Some(&"None"));
            ..set_halign(Align::Start);
        };

        // AI white
        let label_name_white_mode = cascade!{
            Label::new(Some("AI White:"));
            ..set_halign(Align::End);
        };
        let label_value_white_mode = cascade!{
            ComboBoxText::new();
            ..append(Some(&"None"), &"Human");
            ..append(Some(&"3"), &"AI Easy");
            ..append(Some(&"4"), &"AI Medium");
            ..append(Some(&"5"), &"AI Difficult");
            ..set_active_id(Some(&"None"));
            ..set_halign(Align::Start);
        };

        // Play/pause AI
        let button_play_pause = gtk::Button::from_icon_name(Some("media-playback-start"), gtk::IconSize::SmallToolbar);
        button_play_pause.set_sensitive(false);


        let container = cascade!{
            Grid::new();
            ..set_row_homogeneous(true);
            ..set_column_spacing(10);
            ..attach(&label_name_player, 0, 0, 1, 1);
            ..attach(&label_value_player, 1, 0, 1, 1);
            ..attach(&label_name_black, 0, 1, 1, 1);
            ..attach(&label_value_black, 1, 1, 1, 1);
            ..attach(&label_name_white, 0, 2, 1, 1);
            ..attach(&label_value_white, 1, 2, 1, 1);
            ..attach(&label_name_nocapture, 0, 3, 1, 1);
            ..attach(&label_value_nocapture, 1, 3, 1, 1);
            ..attach(&label_name_black_mode, 0, 4, 1, 1);
            ..attach(&label_value_black_mode, 1, 4, 1, 1);
            ..attach(&label_name_white_mode, 0, 5, 1, 1);
            ..attach(&label_value_white_mode, 1, 5, 1, 1);
            ..attach(&button_play_pause, 0, 6, 2, 1);
        };

        Self {
            container,
            info_player: label_value_player,
            info_nocapture: label_value_nocapture,
            info_black: label_value_black,
            info_white: label_value_white,
            combo_black_mode: label_value_black_mode,
            combo_white_mode: label_value_white_mode,
            button_play_pause,
        }
    }

    pub fn update(&self, info: &GameInfo) {
        self.info_player.set_text(&utils::player_to_char(info.player_turn).to_string());
        self.info_black.set_text(&info.score[0].to_string());
        self.info_white.set_text(&info.score[1].to_string());
        self.info_nocapture.set_text(&info.no_take_count.to_string());
        self.combo_black_mode.set_active_id(Some(info.get_player_mode(-1).as_ref()));
        self.combo_white_mode.set_active_id(Some(info.get_player_mode(1).as_ref()));
    }
}
