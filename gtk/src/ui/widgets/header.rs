use gtk::prelude::*;
use gio::ActionMapExt;
// use gtk::Pixbuf;
// use std::rc::Rc;
// use std::cell::RefCell;
// use crate::ui::State;
// use std::ops::Deref;

pub struct Header {
    pub container: gtk::HeaderBar,
    pub button_new: gtk::Button,
    pub button_open: gtk::Button,
    pub button_save: gtk::Button,
    pub button_hint: gtk::Button,
    // state: Rc<RefCell<State>>,
}

// impl AsRef<gtk::HeaderBar> for Header {
//     fn as_ref(&self) -> &gtk::HeaderBar {
//         &self.container
//     }
// }
//
//  impl Deref for Header {
//     type Target = gtk::HeaderBar;
//     fn deref(&self) -> &gtk::HeaderBar {
//         &self.container
//     }
// }

impl Header {
    pub fn new(/*state: Rc<RefCell<State>>*/) -> Self {
        // let menu_box = cascade! {
        //     menu_box: gtk::Box::new(gtk::Orientation::Vertical, 5);
        //     ..pack_start(&gtk::Label::new("Show".into()), false, false, 0);
        // };

        // let popover = cascade! {
        //     gtk::PopoverMenu::new();
        //     ..add(&menu_box);
        //     | menu_box.show_all();
        // };

        let button_new = cascade!{
            gtk::Button::from_icon_name(Some("document-new-symbolic"), gtk::IconSize::Menu);
        };
        let button_open = cascade!{
            gtk::Button::from_icon_name(Some("document-open-symbolic"), gtk::IconSize::Menu);
        };
        let button_save = cascade!{
            gtk::Button::from_icon_name(Some("document-save-symbolic"), gtk::IconSize::Menu);
        };

        let button_hint = cascade!{
            gtk::Button::with_label("Hint");
            ..get_style_context().add_class("suggested-action");
        };


        let container = cascade! {
            gtk::HeaderBar::new();
            ..set_show_close_button(true);
            ..set_title("Latrunculi".into());
            ..pack_start(&button_new);
            ..pack_start(&button_open);
            ..pack_start(&button_save);
            ..pack_end(&build_menu());
            ..pack_end(&button_hint);
        };

        Self {
            container,
            button_new,
            button_open,
            button_save,
            button_hint,
            // state
        }
    }

}

fn build_menu() -> gtk::MenuButton {

    let menu_model = cascade! {
        gio::Menu::new();
        ..append(Some("Game rules"), Some("win.rules"));
        ..append(Some("About"), Some("win.about"));
    };

    let action_rules = cascade! {
        gio::SimpleAction::new("rules", None);
        ..connect_activate(dialog_rules);
    };

    let action_about = cascade! {
        gio::SimpleAction::new("about", None);
        ..connect_activate(dialog_about);
    };

    let action_group = cascade! {
        gio::SimpleActionGroup::new();
        ..add_action(&action_rules);
        ..add_action(&action_about);
    };

    let menu = cascade! {
        gtk::MenuButton::new();
        ..set_image(Some(&gtk::Image::from_icon_name(Some("open-menu-symbolic"), gtk::IconSize::Menu)));
        ..set_menu_model(Some(&menu_model));
        ..insert_action_group("win", Some(&action_group));
    };


    menu
}

fn dialog_rules(_: &gio::SimpleAction, _: Option<&glib::Variant>) {
    let rules = gio::resources_lookup_data("/org/gtk/rules.txt", gio::ResourceLookupFlags::empty()).unwrap();
    let rules = String::from_utf8(rules.to_vec()).unwrap();

    cascade! {
        gtk::MessageDialog::new(
            None::<&gtk::Window>,
            gtk::DialogFlags::empty(),
            gtk::MessageType::Info,
            gtk::ButtonsType::Ok,
            &rules
        );
        ..connect_response(|dialog, _response| { dialog.close(); });
        ..show_all();
    };
}

fn dialog_about(_: &gio::SimpleAction, _: Option<&glib::Variant>) {
    cascade! {
        gtk::AboutDialog::new();
        ..set_authors(&["Daniel Čampiš"]);
        ..set_license_type(gtk::License::Gpl30);
        ..connect_response(|dialog, _response| { dialog.close(); });
        ..show_all();
    };
}
