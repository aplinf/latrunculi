use gtk::prelude::*;
use std::f64::consts::PI;
use std::rc::Rc;
use std::cell::RefCell;

use crate::State;

use latrunculi::Board as BoardLib;

pub struct Board {
    pub container: gtk::AspectFrame,
    pub drawing_area: gtk::DrawingArea,
    pub state: Rc<RefCell<State>>,
    draw_buf: Rc<RefCell<Option<BoardLib>>>,
}

impl Board {
    pub fn new(state: Rc<RefCell<State>>) -> Self {
        let drawing_area = gtk::DrawingArea::new();
        let draw_buf = Rc::new(RefCell::new(None::<BoardLib>));

        // drawing_area.set_size_request(500, 500);
        // drawing_area.set_valign(gtk::Align::Center);

        drawing_area.add_events(gdk::EventMask::BUTTON_PRESS_MASK);

        // area.add_events(gdk::EventMask::POINTER_MOTION_MASK);
        // area.add_events(gdk::EventMask::BUTTON_RELEASE_MASK);


        // let board_state = state.clone();
        let board_state = draw_buf.clone();

        drawing_area.connect_draw(move |w, c| {
            let board = board_state.borrow();
            let width = w.get_allocated_width() as f64 / 8.0;
            let height = w.get_allocated_height() as f64 / 7.0;
            let cell_size = if width < height {width} else {height};

            c.set_line_width(4.);

            for row in 0..7 {
                let row_f = row as f64;

                for col in 0..8 {
                    let col_f = col as f64;

                    match (row + col) % 2 {
                        0 => c.set_source_rgb(0.498, 0.333, 0.224),
                        1 => c.set_source_rgb(0.929, 0.878, 0.831),
                        _ => (),
                    }

                    c.rectangle(cell_size * col_f, cell_size * row_f, cell_size, cell_size);
                    c.fill();

                    if let Some(board) = board.as_ref() {
                        if board.matrix[row][col] != 0 {
                            match board.matrix[row][col] {
                                -1 => c.set_source_rgb(0.3, 0.3, 0.3),
                                1 => c.set_source_rgb(0.9, 0.9, 0.9),
                                _ => (),
                            }

                            c.arc(cell_size * col_f + cell_size / 2., cell_size * row_f + cell_size / 2., cell_size * 0.4, 0., 2. * PI);
                            c.fill_preserve();

                            match board.matrix[row][col] {
                                -1 => c.set_source_rgb(0.25, 0.25, 0.25),
                                1 => c.set_source_rgb(0.7, 0.7, 0.7),
                                _ => (),
                            }

                            c.stroke();
                        }
                    }
                }
            }

            if let Some(board) = board.as_ref() {
                if let Some(proposed) = board.proposed {
                    c.set_line_width(6.);
                    c.set_source_rgb(0.9, 0.8, 0.3);
                    c.rectangle(cell_size * proposed[0][0] as f64, cell_size * proposed[0][1] as f64, cell_size, cell_size);
                    c.rectangle(cell_size * proposed[1][0] as f64, cell_size * proposed[1][1] as f64, cell_size, cell_size);
                    c.stroke();

                }

                if let Some(selected) = board.selected {
                    c.set_source_rgb(0., 0.9, 0.);
                    c.rectangle(cell_size * selected[0] as f64, cell_size * selected[1] as f64, cell_size, cell_size);
                    c.stroke();

                }
            }

            gtk::Inhibit(false)
        });

        // drawing_area.set_size_request(drawing_area.get_allocated_width(), drawing_area.get_allocated_height());

        let container = cascade! {
            board_box: gtk::AspectFrame::new(
                None,
                0.5, 0.5,
                8./7.,
                false,
            );
            ..add(&drawing_area);
            ..set_widget_name("board-box");
            ..set_size_request(250, 250);
            // | board_box.set_widget_name("board-box");
        };

        Self {
            container,
            drawing_area,
            draw_buf,
            state,
        }
    }

    pub fn draw(&self, board: BoardLib) {
        let state = self.state.borrow();

        let mut draw_buf = self.draw_buf.borrow_mut();
        *draw_buf = Some(state.game.board.clone());

        self.drawing_area.queue_draw();
    }

    pub fn get_clicked_cell(&self, click: (f64, f64)) -> Option<[i8; 2]> {
        let width = self.drawing_area.get_allocated_width();
        let height = self.drawing_area.get_allocated_height();

        let cell_size = if width / 8 < height / 7 {width / 8} else {height / 7};
        let col = click.0 as i32 / cell_size;
        let row = click.1 as i32 / cell_size;

        if col < 8 && row < 7 {
            Some([col as i8, row as i8])
        } else { None }

    }
}
