use gtk::prelude::*;
use gtk::TreePath;
use glib::types::Type;

use latrunculi::{GameInfo, HistItem, History as HistoryData};
use latrunculi::utils;

pub struct History {
    pub container: gtk::Box,
    pub history_view: gtk::TreeView,
    pub history_store: gtk::ListStore,
    pub button_undo: gtk::Button,
    pub button_redo: gtk::Button,
}

impl History {
    pub fn new() -> Self {
        let history_view = gtk::TreeView::new();

        let history_store = gtk::ListStore::new(&[Type::String, Type::String, Type::U8]);
        history_view.set_model(Some(&history_store));

        let mut column = gtk::TreeViewColumn::new();
        let mut cell = gtk::CellRendererText::new();

        column.pack_end(&cell, true);
        column.add_attribute(&cell, "text", 0);
        history_view.append_column(&column);

        column = gtk::TreeViewColumn::new();
        cell = gtk::CellRendererText::new();

        column.pack_end(&cell, true);
        column.add_attribute(&cell, "text", 1);
        column.set_title("Moves");
        history_view.append_column(&column);

        column = gtk::TreeViewColumn::new();
        cell = gtk::CellRendererText::new();

        column.pack_end(&cell, true);
        column.add_attribute(&cell, "text", 2);
        column.set_title("Captured");
        history_view.append_column(&column);

        let container = cascade! {
            gtk::ScrolledWindow::new(None::<&gtk::Adjustment>, None::<&gtk::Adjustment>);
            ..add(&history_view);
            ..set_size_request(200, -1);
        };

        let button_undo = gtk::Button::from_icon_name(Some("go-previous"), gtk::IconSize::SmallToolbar);
        let button_redo = gtk::Button::from_icon_name(Some("go-next"), gtk::IconSize::SmallToolbar);

        let toolbar = cascade! {
            gtk::Toolbar::new();
            ..insert(&cascade! {
                gtk::ToolItem::new();
                ..add(&button_undo);
            }, -1);
            ..insert(&cascade! {
                gtk::ToolItem::new();
                ..add(&button_redo);
            }, -1);
        };

        let container = cascade! {
            gtk::Box::new(gtk::Orientation::Vertical, 0);
            ..pack_start(&container, true, true, 0);
            ..pack_start(&toolbar, false, false, 0);
        };


        Self {
            container,
            history_view,
            history_store,
            button_undo,
            button_redo,
        }
    }

    // update history view on every move
    // pub fn update(&self, info: &GameInfo) {
    //     let history_view_len = self.history_store.iter_n_children(None);
    //     let (hist_item, hist_idx) = info.history;
    //
    //     // if history state was truncated, remove old rows
    //     if hist_idx == -1 {
    //         self.history_store.clear();
    //     } else if hist_idx != history_view_len {
    //         let store_iter = self.history_store.get_iter(
    //             &TreePath::from_indicesv(&[0])
    //         ).unwrap();
    //
    //         for _ in hist_idx..history_view_len {
    //             self.history_store.remove(&store_iter);
    //         }
    //     }
    //
    //     if let Some(hist_item) = hist_item {
    //         self.insert_row(&hist_item);
    //     }
    // }

    pub fn update(&self, game_info: &GameInfo) {
        self.history_store.clear();

        for hist_item in game_info.history.steps.iter() {
            // println!("{:?}", hist_item);
            self.insert_row(hist_item);
        }
    }

    pub fn replace(&self, history: &HistoryData) {
        self.history_store.clear();

        for hist_item in history.steps.iter() {
            self.insert_row(hist_item);
        }
        // upload all history!!!> for hist_item in info.history
    }

    fn insert_row(&self, hist_item: &HistItem) {
        self.history_store.insert_with_values(None,
            &[0, 1, 2],
            &[
                &utils::player_to_char(hist_item.3).to_string(),
                &format_move(&hist_item),
                &(hist_item.1.iter().fold(0, |acc, x| acc + x.abs()) as u8),
            ]
        );

        // Set selected row to last
        self.set_cursor(self.history_store.iter_n_children(None) - 1);
    }

    pub fn set_cursor(&self, idx: i32) {
        self.history_view.set_cursor(
            &TreePath::from_indicesv(&[idx]),
            None::<&gtk::TreeViewColumn>,
            false
        );
    }
}

fn format_move(hist_item: &HistItem) -> String {
    format!("{}{} ⇨ {}{}",
        (hist_item.0[0][0] as u8 + 65) as char,
        hist_item.0[0][1] + 1,
        (hist_item.0[1][0] as u8 + 65) as char,
        hist_item.0[1][1] + 1,
    )
}
