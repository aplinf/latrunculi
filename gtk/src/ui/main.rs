use gtk::prelude::*;
use std::rc::Rc;
use std::cell::RefCell;
use super::State;

use super::widgets::Board;
use super::widgets::History;
use super::widgets::Status;

pub struct Main {
    pub container: gtk::Box,
    pub board: Board,
    pub history: History,
    pub status: Status,
    pub state: Rc<RefCell<State>>
}

impl Main {
    pub fn new(state: Rc<RefCell<State>>) -> Self {
        let board = Board::new(state.clone());
        let history = History::new();
        let status = Status::new();

        let container_right = cascade! {
            gtk::Box::new(gtk::Orientation::Vertical, 0);
            ..pack_start(&status.container, false, false, 20);
            ..pack_start(&history.container, true, true, 0);
        };

        let container = cascade! {
            gtk::Box::new(gtk::Orientation::Horizontal, 0);
            ..pack_start(&board.container, true, true, 0);
            ..pack_end(&container_right, false, true, 0);
        };

        Self {
            container,
            board,
            history,
            status,
            state,
        }
    }
}
