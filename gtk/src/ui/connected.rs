use flume::Sender;
use gtk::prelude::*;
use std::path::PathBuf;
use latrunculi::Move;
// use gtk::DrawingArea;
// use cairo::context::Context;

use super::App;

pub enum Button {
    Hint,
    NewGame,
    Undo,
    Redo,
}

pub enum AppEvent {
    // GetBestMove,
    ShowHint(Option<Move>),
    SelectBoard((f64, f64)),
    SelectHistory(i32),
    FileDialog(gtk::FileChooserAction),
    OpenGame(PathBuf),
    SaveGame(PathBuf),
    ClickButton(Button),
    SetPlayerMode((i8, Option<u8>)),
    PlayAI,
    PlayMove(Option<Move>),
    Update,
    PlayPause,
}


impl App{
    pub fn connect_events(&self, sender: Sender<AppEvent>) {
        self.connect_new_game(sender.clone());
        self.connect_open_game(sender.clone());
        self.connect_save_game(sender.clone());
        self.connect_hint(sender.clone());
        self.connect_select_history(sender.clone());
        self.connect_set_player_mode(sender.clone());
        self.connect_play_pause(sender.clone());
        self.connect_button_undo(sender.clone());
        self.connect_button_redo(sender.clone());
        self.connect_select_board_cell(sender);
    }

    fn connect_new_game(&self, sender: Sender<AppEvent>) {
        self.header.button_new.connect_clicked(move |_| {
            sender.send(AppEvent::ClickButton(Button::NewGame)).unwrap();
        });
    }
    fn connect_open_game(&self, sender: Sender<AppEvent>) {
        self.header.button_open.connect_clicked(move |_| {
            // sender.send(AppEvent::OpenGame).unwrap();
            sender.send(AppEvent::FileDialog(gtk::FileChooserAction::Open)).unwrap();
        });
    }
    fn connect_save_game(&self, sender: Sender<AppEvent>) {
        self.header.button_save.connect_clicked(move |_| {
            // sender.send(AppEvent::SaveGame).unwrap();
            sender.send(AppEvent::FileDialog(gtk::FileChooserAction::Save)).unwrap();
        });
    }
    fn connect_hint(&self, sender: Sender<AppEvent>) {
        self.header.button_hint.connect_clicked(move |_| {
            // sender.send(AppEvent::SaveGame).unwrap();
            sender.send(AppEvent::ClickButton(Button::Hint)).unwrap();
        });
    }

    fn connect_select_history(&self, sender: Sender<AppEvent>) {
        self.main.history.history_view.connect_cursor_changed(move |w| {
            let selected = w.get_selection().get_selected_rows().0;

            // do nothing if user select nothing (unselect preselected)
            if selected.len() > 0 {
                sender.send(AppEvent::SelectHistory(selected[0].get_indices()[0])).unwrap();
            }
        });
    }

    fn connect_set_player_mode(&self, sender: Sender<AppEvent>) {
        let sender_clone = sender.clone();
        let get_depth = |active_id: Option<glib::GString>|
            match active_id.unwrap().as_str() {
                "None" => None,
                "1" => Some(1),
                "2" => Some(2),
                "3" => Some(3),
                "4" => Some(4),
                "5" => Some(5),
                _ => None,
            };

        self.main.status.combo_black_mode.connect_changed(move |w| {
            sender.send(AppEvent::SetPlayerMode((-1, get_depth(w.get_active_id())))).unwrap();
        });

        self.main.status.combo_white_mode.connect_changed(move |w| {
            sender_clone.send(AppEvent::SetPlayerMode((1, get_depth(w.get_active_id())))).unwrap();
        });
    }

    fn connect_play_pause(&self, sender: Sender<AppEvent>) {
        self.main.status.button_play_pause.connect_clicked(move |_| {
            sender.send(AppEvent::PlayPause).unwrap();
        });
    }

    fn connect_button_undo(&self, sender: Sender<AppEvent>) {
        self.main.history.button_undo.connect_clicked(move |_| {
            sender.send(AppEvent::ClickButton(Button::Undo)).unwrap();
        });
    }

    fn connect_button_redo(&self, sender: Sender<AppEvent>) {
        self.main.history.button_redo.connect_clicked(move |_| {
            sender.send(AppEvent::ClickButton(Button::Redo)).unwrap();
        });
    }

    fn connect_select_board_cell(&self, sender: Sender<AppEvent>) {
        self.main.board.drawing_area.connect_button_press_event(move |_, e| {
            sender.send(AppEvent::SelectBoard(e.get_position())).unwrap();

            gtk::Inhibit(false)
        });
    }
}
