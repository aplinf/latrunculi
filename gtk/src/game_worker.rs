use std::thread;

use latrunculi::Game;
use latrunculi::utils;

pub enum GameEvent {
    Log,
}

pub fn init() -> flume::Sender<GameEvent> {
    let (tx, rx) = flume::unbounded();

    thread::spawn(move || {
        let game = Game::new();

        while let Ok(event) = rx.recv() {
            match event {
                GameEvent::Log => {
                    utils::sleep(5);
                    println!(">> worker: log");
                },
            }
        }
    });

    tx
}
