use gtk::prelude::*;
use gio::prelude::*;
use latrunculi::{Game, GameInfo, utils, Move};

use std::rc::Rc;
use std::cell::RefCell;
use std::path::PathBuf;
use std::thread;

mod main;
mod widgets;
mod connected;

use self::main::Main;
use self::widgets::Header;
pub use self::connected::{AppEvent, Button};

pub struct State {
    pub app_tx: flume::Sender<AppEvent>,
    pub game: Game,
    pub is_paused: bool,
}

pub struct App {
    pub window: gtk::Window,
    pub header: Header,
    pub main: Main,
    pub state: Rc<RefCell<State>>,
}

impl App {
    pub fn new(state: State) -> Self {
        resources_register();

        let css_provider = gtk::CssProvider::new();
        css_provider.load_from_data(include_bytes!("styles.css")).unwrap();

        gtk::StyleContext::add_provider_for_screen(
            &gdk::Screen::get_default().expect("Error initializing gtk css provider."),
            &css_provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );

        let state = Rc::new(RefCell::new(state));

        let header = Header::new();
        let main = Main::new(state.clone());

        let window = cascade! {
            gtk::Window::new(gtk::WindowType::Toplevel);
            | gtk::Window::set_default_icon(&get_app_icon());
            ..set_titlebar(Some(&header.container)); // better impement header.as_ref() ?
            ..set_title("Latrunculi");
            ..set_default_size(700, 700);
            ..add(&main.container);

            ..connect_delete_event(move |_, _| {
                gtk::main_quit();
                Inhibit(false)
            });

            ..connect_key_press_event(move |_, event_key| {
                if let Some('\u{1b}') = event_key.get_keyval().to_unicode() {
                    gtk::main_quit();
                }

                Inhibit(false)
            });
        };


        let app = Self {
            window,
            header,
            main,
            state,
        };

        app.state.borrow().app_tx.send(AppEvent::Update).unwrap();

        app
    }

    pub fn show(&self) {
        self.window.show_all();
    }

    pub fn new_game(&self) {
        let mut state = self.state.borrow_mut();
        state.game.reinit();

        state.app_tx.send(AppEvent::Update).unwrap();
    }

    pub fn file_dialog(&self, action: gtk::FileChooserAction) {
        let state = self.state.borrow();
        let sender = state.app_tx.clone();
        let confirm_text = match action {
            gtk::FileChooserAction::Open => "Open",
            gtk::FileChooserAction::Save => "Save",
            _ => "Confirm",
        };

        cascade!{
            gtk::FileChooserDialog::new(None, Some(&self.window), action);
            ..add_buttons(&[
                (confirm_text, gtk::ResponseType::Ok),
                ("Cancel", gtk::ResponseType::Cancel)
            ]);
            ..connect_response(move |dialog, response| {
                if response == gtk::ResponseType::Ok {
                    if let Some(path) = dialog.get_filenames().last() {
                        match action {
                            gtk::FileChooserAction::Open => sender.send(AppEvent::OpenGame(path.to_path_buf())).unwrap(),
                            gtk::FileChooserAction::Save => sender.send(AppEvent::SaveGame(path.to_path_buf())).unwrap(),
                            _ => (),

                        };
                    }
                }

                dialog.close();
            });
            ..show_all();
        };
    }

    pub fn open_game(&self, path: PathBuf) {
        match gio_read_file(path) {
            Ok(file_string) => {
                let mut state = self.state.borrow_mut();

                match state.game.import(file_string) {
                    Ok(_) => self.pop_message("File imported"),
                    Err(err) => {
                        eprintln!("error: {}", err);
                        self.pop_message("File import failed (the file is wrong or corrupted)")
                    },
                };

                state.app_tx.send(AppEvent::Update).unwrap();
            },
            Err(err) => {
                eprintln!("error: {}", err);
                self.pop_message("File import failed (can not open the file)");
            },
        };
    }

    pub fn save_game(&self, path: PathBuf) {
        let content = self.state.borrow().game.export();

        match gio_write_file(path, content) {
            Ok(_) => self.pop_message("File saved"),
            Err(_) => self.pop_message("File save failed"),
        };
    }

    // Game play
    pub fn select_board_cell(&mut self, cursor_pos: (f64, f64)) {
        let mut state = self.state.borrow_mut();
        let mut was_moved = false;

        if let Some(cell_pos) = self.main.board.get_clicked_cell(cursor_pos) {
            if let Some(game_over) = state.game.select(cell_pos) {
                was_moved = true;

                if let Some(finish_state) = game_over {
                    self.finish_game(finish_state);
                }

                self.main.history.update(&state.game.get_game_info());
            }

            state.app_tx.send(AppEvent::Update).unwrap();
        }

        if was_moved {state.app_tx.send(AppEvent::PlayAI).unwrap();}
    }

    pub fn finish_game(&self, finish_state: i8) {
        match finish_state {
            -1 => self.pop_message("Black won!"),
            1 => self.pop_message("White won!"),
            _ => self.pop_message("Draw"),
        };
    }

    pub fn get_best_move_and(&self, action: AppEvent) {
        let state = self.state.borrow_mut();

        let mut game_clone = state.game.clone();
        let tx = state.app_tx.clone();

        thread::spawn(move || {
            let best_move = game_clone.best_move();

            let event = match action {
                AppEvent::ShowHint(_) => AppEvent::ShowHint(best_move),
                AppEvent::PlayMove(_) => AppEvent::PlayMove(best_move),
                _ => AppEvent::Update,
            };

            tx.send(event).unwrap();
        });
    }

    pub fn play_ai(&self) {
        if {
            let state = self.state.borrow();
            !state.is_paused && state.game.is_player_ai()
        } {
            self.get_best_move_and(AppEvent::PlayMove(None));
        }
    }

    pub fn play_move(&self, move_n: Option<Move>) {
        let mut is_game_over = false;

        {
            let mut state = self.state.borrow_mut();

            if !state.is_paused && state.game.is_player_ai() {
                if let Some(move_n) = move_n {
                    if let Some(game_over) = state.game.play_move(move_n, true) {
                        is_game_over = true;
                        self.finish_game(game_over);
                    } else {
                        state.app_tx.send(AppEvent::Update).unwrap();

                        if state.game.is_player_ai() {
                            state.app_tx.send(AppEvent::PlayAI).unwrap();
                        }
                    }

                    self.main.history.update(&state.game.get_game_info());
                }
            }
        }

        if is_game_over { self.pause(); }
    }

    pub fn show_hint(&self, move_n: Option<Move>) {
        let mut state = self.state.borrow_mut();

        state.game.board.set_proposed(move_n);

        state.app_tx.send(AppEvent::Update).unwrap();
    }

    // History
    pub fn select_history(&self, idx: i32) {
        let mut state = self.state.borrow_mut();

        // if user selects already selected do nothing
        if state.game.history.current_step != idx {
            state.game.set_history(idx);

            state.app_tx.send(AppEvent::Update).unwrap();
        }
    }

    pub fn undo(&self) {
        let mut state = self.state.borrow_mut();
        let current_step = state.game.history.current_step;

        if current_step >= 0 {
            state.game.set_history(current_step - 1);

            if current_step > 0 {
                self.main.history.set_cursor(current_step - 1);
            }

            state.app_tx.send(AppEvent::Update).unwrap();
        }
    }

    pub fn redo(&self) {
        let mut state = self.state.borrow_mut();
        let current_step = state.game.history.current_step;
        let last_step = state.game.history.last_idx();

        if current_step < last_step {
            state.game.set_history(current_step + 1);

            self.main.history.set_cursor(current_step + 1);

            state.app_tx.send(AppEvent::Update).unwrap();
        }

    }

    // Player mode
    pub fn set_player_mode(&self, player_mode: (i8, Option<u8>)) {
        self.pause();

        let mut state = self.state.borrow_mut();

        state.game.set_player_mode(player_mode);

        if state.game.player_mode[0] == None &&
           state.game.player_mode[1] == None {
            self.main.status.button_play_pause.set_sensitive(false);
        } else {
            self.main.status.button_play_pause.set_sensitive(true);
        }
    }

    pub fn toggle_play_pause(&self) {
        if {
            let state = self.state.borrow();

            state.game.is_game_over() == None &&
            (state.game.player_mode[0] != None ||
            state.game.player_mode[1] != None)
        } {
            if self.state.borrow().is_paused {
                self.play();
            } else {
                self.pause();
            }
        };
    }

    pub fn play(&self) {
        {
            let mut state = self.state.borrow_mut();
            state.is_paused = false;

            self.main.status.button_play_pause.set_image(Some(
                &gtk::Image::from_icon_name(Some("media-playback-pause"), gtk::IconSize::SmallToolbar)
            ));

            state.app_tx.clone()
        }.send(AppEvent::PlayAI).unwrap();
    }

    pub fn pause(&self) {
        let mut state = self.state.borrow_mut();
        state.is_paused = true;

        self.main.status.button_play_pause.set_image(Some(
            &gtk::Image::from_icon_name(Some("media-playback-start"), gtk::IconSize::SmallToolbar)
        ));
    }

    // Other
    pub fn pop_message(&self, message: &str) {
        cascade!{
            gtk::MessageDialog::new(
                Some(&self.window),
                gtk::DialogFlags::empty(),
                gtk::MessageType::Info,
                gtk::ButtonsType::Ok,
                message
            );
            ..show_all();
            ..connect_response(|w, _| {
                w.close();
            });
        };
    }

    pub fn update(&self) {
        let state = self.state.borrow();

        let game_info = state.game.get_game_info();
        // self.main.history.update(&game_info);
        self.main.status.update(&game_info);

        self.main.board.draw(state.game.board.clone());
    }
}


fn resources_register() {
    let bytes = glib::Bytes::from(include_bytes!("../resources/app.gresource"));
    let resource = gio::Resource::from_data(&bytes).unwrap();
    gio::resources_register(&resource);
}

fn get_app_icon() -> gdk_pixbuf::Pixbuf {
    gdk_pixbuf::Pixbuf::from_resource("/org/gtk/icons/icon.svg").unwrap()
}

fn gio_read_file(file_path: PathBuf) -> Result<String, Box<dyn std::error::Error>> {
    let mut buf = String::new();
    let stream = gio::File::new_for_path(&file_path)
                    .read(None::<&gio::Cancellable>)?;

    loop {
        let content = stream.read_bytes(2048, None::<&gio::Cancellable>)?;

        if content.len() > 0 {
            buf.push_str(glib::String::new(content).to_str()?);
        } else {
            break;
        }
    }

    Ok(buf)
}

fn gio_write_file(file_path: PathBuf, content: String) -> Result<(), Box<dyn std::error::Error>> {
    let stream = gio::File::new_for_path(&file_path)
                        .replace(
                            None,
                            false,
                            gio::FileCreateFlags::NONE,
                            None::<&gio::Cancellable>
                        )?;

    stream.write(content.as_bytes(), None::<&gio::Cancellable>)?;
    stream.close(None::<&gio::Cancellable>)?;

    Ok(())
}
