# Rust Project Resources

## Compilation

### Linkers time comparison

`GNU ld  > GNU gold  > LLD`

### Compiling time

https://endler.dev/2020/rust-compile-times/

https://blog.mozilla.org/nnethercote/2020/04/24/how-to-speed-up-the-rust-compiler-in-2020/

### Cargo compilation benchmarking

`cargo rustc -- -Ztime-passes`



## Crates

### Termion

* https://docs.rs/termion/1.5.5/termion/index.html
* https://github.com/redox-os/termion

## TUI Tutorials

### To read

#### Good IO Rust tutorial

* https://www.philippflenker.com/hecto-chapter-2/

### Basic Termion tutorial 

* http://ticki.github.io/blog/making-terminal-applications-in-rust-with-termion/
* Includes all main parh
* Has a lot of example project

### Github: Redox games

	* https://github.com/redox-os/games
	* Nice collection of games for code inspiration

### TUI-RS tutorial

* https://qiita.com/wangya_eecs/items/b9e1a501cb0c0ab0de1c
* ! This crate was abandoned for excessive complexity

## GTK

### GTK Devtool

`GTK_DEBUG=interactive`



https://mmstick.github.io/gtkrs-tutorials

https://www.cairographics.org/tutorial/

### Widgets: list

https://developer.gnome.org/gtk3/stable/index.html

### Widgets: gallery

https://developer.gnome.org/gtk3/stable/ch03.html

### CSS: refs

https://developer.gnome.org/gtk3/stable/chap-css-properties.html



* 