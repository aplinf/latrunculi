## Algoritmus Minimax
```
function minimax(pozice, hloubka)

if je prohra(pozice) then
    return −MAX
end if

if je výhra(pozice) then
    return MAX
end if

if je_remı́za(pozice) then
    return 0
end if

if hloubka = 0 then
    return ohodnocovaci funkce(pozice)
else
    tahy ← generuj tahy(pozice)
    ohodnoceni ← −MAX

    for all tah v kolekci tahy do
        potomek ← zahraj(pozice, tah)
        ohodnoceni ← max(ohodnoceni, −minimax(potomek, hloubka − 1))
    end for

    if ohodnoceni > MNOHO then
        ohodnoceni ← ohodnoceni − 1
    end if

    if ohodnoceni < −MNOHO then
        ohodnoceni ← ohodnoceni + 1
    end if

    return ohodnoceni
end if

end function
```

## Zjištěnı́ nejlepšı́ho tahu
```
function nej tah(pozice, hloubka)
  tahy ← generuj tahy(pozice)
  nejlepsi ohodnoceni ← −MAX

  for all tah v kolekci tahy do
    potomek ← zahraj(pozice, tah)
    ohodnoceni ← −minimax(potomek, hloubka − 1)

    if ohodnoceni > nejlepsi ohodnoceni then
      nejlepsi ohodnoceni ← ohodnoceni
      nejlepsi tah ← tah
    end if
  end for

  return nejlepsi tah
end function
```
