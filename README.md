## Požadavky za zimní semestr (pro zápočet z KMI/YPS1)
- [x] stačí konzolová (textová, CLI) aplikace, není nutné GUI
- [x] korektní implementace pravidel hry (nemožnost provést tah odporující pravidlům)
- [x] správné ukončení hry
- [x] algoritmy pro herní strategii, nastavitelná obtížnost hry v adekvátním rozsahu („inteligence“ zvlášť pro každého počítačového hráče)
- [x] výpočet „nejlepšího“ tahu by neměl trvat déle než 10 sekund (na počítači s CPU 2 GHz)
- [x] možnost hry dvou lidí, člověka proti „počítači“, a „počítače“ proti „počítači“
- [x] možnost nastavit a kdykoliv změnit obtížnost i v průběhu hry
- [x] možnost zaměnit počítačového a lidského hráče i v průběhu hry
- [x] nápověda „nejlepšího“ tahu

## Požadavky za letní semestr (pro zápočet z KMI/YPS2)
- [x] undo/redo tahů do libovolné úrovně
- [x] prohlížení historie tahů (přehledné zobrazení provedených tahů) se zobrazením úrovně undo/redo
- [x] ukládání a načítání (rozehraných i ukončených) partií hry, včetně historie tahů a nastavení hráčů
- [x] grafické uživatelské rozhraní (GUI) zpracované podle standardů
- [x] GUI musí běžně reagovat na akce uživatele i v průběhu výpočtu „nejlepšího“ tahu
- [x] vestavěná nápověda (k ovládání aplikace i pravidlům hry)
- [x] robustnost (program musí reagovat správně i na nesprávné uživatelské vstupy, vadný formát nebo obsah souboru apod., aplikace nesmí havarovat)
- [x] program ve spustitelné formě nebo instalátor (v odůvodněných případech je přípustná spustitelnost z vývojového prostředí)
- [x] kompletní zdrojové kódy programu včetně dalších částí nutných pro sestavení aplikace
